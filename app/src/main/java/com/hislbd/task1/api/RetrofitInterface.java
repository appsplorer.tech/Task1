package com.hislbd.task1.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;

/**
 * Created by kamana on 3/7/17.
 */

public interface RetrofitInterface {

    @GET("repository/master/archive.zip")
    @Streaming
    Call<ResponseBody> downloadFile();
}
